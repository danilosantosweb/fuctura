<?php

	class Funcionario {
		public $nome;
		private $salario;
		private $valorHoraExtra = 150;

		public function receberSalario($nomeFuncionario, $salario){
			echo "O Funcionario " . $nomeFuncionario . " recebeu R$ " . $salario . " de salário<br />";
			$this->nome = $nomeFuncionario;
			$this->salario = $salario;
		}

		public function receberHoraExtra($nomeFuncionario, $qtdHoraExtra){
			$totalHoraExtra = $this->valorHoraExtra * $qtdHoraExtra;
			$this->salario += $totalHoraExtra;
			echo "O Funcionário " . $this->nome . " recebeu R$ " . $totalHoraExtra . " de hora extra. O salário total ficou: R$ " . $this->salario;
		}


	}


?>