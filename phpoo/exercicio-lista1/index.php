<?php
	require_once "classes/Funcionario.php";

	if ($_POST){
		$nome 	 = $_POST['nome'];
		$salario = $_POST['salario'];
		$qtdHoraExtra 	 = $_POST['qtdHoraExtra'];

		$func = new Funcionario;
		$func->receberSalario($nome, $salario);
		$func->receberHoraExtra($nome, $qtdHoraExtra);

	}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Lista de Exercícios I</title>
</head>
<body>
	
	<form action="" method="post">
		<input type="text" name="nome" placeholder="Nome">
		<input type="number" name="salario" placeholder="Salário">
		<input type="number" name="qtdHoraExtra" placeholder="Quantidade de Horas Extras">
		<button type="submit">Enviar</button>
	</form>


</body>
</html>