<?php
	
	class Pessoa {
		protected $nome;
		private $cpf;

		public function mudarNome($nNome){
			$this->nome = $nNome;
		}

		public function mostrarNome(){
			return "Nome da Pessoa: ".$this->nome;
		}
	}

	// A classe "PessoaFisica" por ter extendido a classe "Pessoa", herda todos os seus atributos e métodos
	// Nesse caso, o método mostrarNome() foi sobreescrito
	class PessoaFisica extends Pessoa {
		private $cnpj;

		public function mostrarNome(){
			return "Nome da Empresa: ".$this->nome;
		}



	}

	// Percebam que o atributo nome só foi declarado na classe "Pessoa", mas está sendo usado
	// em ambas as classes.

	$p = new Pessoa;
	$p->mudarNome("Fulano de tal");

	$pf = new PessoaFisica;
	$pf->mudarNome("Fuctura");

	echo $p->mostrarNome();
	echo "<br />";
	echo $pf->mostrarNome();


?>