<?php

	class NomeInvalido extends Exception{}
	class CidadeNaoEncontrada extends Exception{}

	class Cidade {

		// construtor
		function __construct() {
			echo "Testando…";
		}

		function pesquisa($cidade){

			if (strlen($cidade)<=4) {
				throw new NomeInvalido("A cidade precisa ter mais de 4 caracteres");
			}

			$opcoes = array("Araras", "Leme", "São Carlos", "São Paulo");
			if(!in_array($cidade, $opcoes))
				throw new CidadeNaoEncontrada("<br><br>$cidade nao encontrada em nossa base de dados!<br><br>");
			else
				echo "<br />" . "A cidade " . $cidade . " foi encontrada em nossa base de dados";

		}

	}

	$cidade = new Cidade;

	try {
		$cidade->pesquisa("Arara");
	} catch (CidadeNaoEncontrada $e){
		echo ($e->getMessage());
	} catch (NomeInvalido $ni) {
		echo ($ni->getMessage());
	}

?>