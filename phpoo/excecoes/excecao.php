<?php

	class Cidade {

		// construtor
		function __construct() {
			echo "Testando…";
		}

		function pesquisa($cidade){ 
			$opcoes = array("Araras", "Leme", "São Carlos", "São Paulo");
			if(!in_array($cidade, $opcoes))
				throw new Exception("<br><br>$cidade nao encontrada em nossa base de dados!<br><br>");
			else
				echo "<br />" . "A cidade " . $cidade . " foi encontrada em nossa base de dados";

		}

	}

	$cidade = new Cidade;

	try {
		$cidade->pesquisa("Araras");
	} catch (Exception $e){
		echo ($e->getMessage());
	}

?>