<?php 

	class Pessoa {

		private $nome = "Danilo Santos";
		private $idade = "26";

		public function __toString() {
			return "O nome dele é " . $this->nome . " e a idade dele é " . $this->idade;
		}

		public function __set($atributo, $valor){
			return $this->$atributo = $valor;
		}

		
	}

	$p1 = new Pessoa;
	$p1->nome = "Neco";
	$p1->idade = "40";
	echo $p1;

?>