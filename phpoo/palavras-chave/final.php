<?php
	class Pessoa {
		public $nome;
		public $idade;
		
		// esse método não pode ser sobreescrito na classe filha "PessoaFisica",
		// pois está definido como "final".
		// façam o teste tentando criá-lo na classe extendida
		public final function correr(){
			echo "<br />Você está acessando o método correr ";
		}

		public function pular(){
			echo "<br />Estou pulando!";
		}		
	}

	class PessoaFisica extends Pessoa {
		
		public function pular($nome, $altura){
			echo "<br />O aluno " . $nome . " pulou " . $altura . "m";
		}		

	}


	// todos os dois métodos chamados abaixo, são os definidos na classe "Pessoa"
	$pf = new Pessoa;
	$pf->correr();
 
	$pf2 = new PessoaFisica;
	$pf2->correr();
	
	


?>