<?php 

	// Quando definimos uma classe como "abstrata", ela pode conter métodos não abstratos.
	// Porém se definirmos métodos "abstratos", obrigatoriamente nossa classe deve ser abstrata.
	// OBRIGATORIAMENTE, todos os métodos "abstratos" não podem ter "corpo" e devem ser sobreescritos
	// nas classes filhas.
	abstract class Pessoa {
		public static $nome;

		public abstract function pular();
		public static function correr(){
			echo "<br />Você está acessando o método correr ";
		}

	}

	class PessoaFisica extends Pessoa {
		public function pular(){

		}
	}

	Pessoa::$nome = "Danilo Santos";	
	echo Pessoa::$nome;

	Pessoa::correr();


?>