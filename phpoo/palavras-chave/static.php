<?php 

	class Pessoa {

		// Atributos ou métodos declarados como estáticos faz dele acessíveis
		// sem precisar instanciar uma classe com a palavra chave "new"

		public static $nome;
		
		// Como não instanciamos a classe, consequentemente não utilizamos a pseudo variável "$this",
		// ao invés disso utilizaremos o "self".
		public static function mostrarNome(){
			return self::$nome;
		}


	}

	
	Pessoa::$nome = "Bruno";
	echo Pessoa::mostrarNome();


?>