<?php 

	class Pessoa {
		private $nome;

		// método chamada no momento em que um objeto é instanciado
		public function __construct(){
			echo "Eu sou o construtor"."<br />";
		}

		// método chamada no momento em que um objeto é destruído ou setado como null
		public function __destruct(){
			echo "<br />"."Eu sou o destruct"."<br />";
		}		

	}	

	$pf = new Pessoa;	

?>